import { Component } from '@angular/core';

@Component({
  selector: 'pipeline-jest-test-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'application-name';
}
