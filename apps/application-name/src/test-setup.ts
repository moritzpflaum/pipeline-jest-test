import 'jest-preset-angular/setup-jest';

console.error = (message?: unknown, ...optionalParams: unknown[]): void => {
  if (typeof message === 'string' && message.includes('NG0304')) {
    throw new Error(message);
  } else {
    console.warn({ message, optionalParams });
  }
};
